@extends('layouts.app')

@section('content')
    <section class="orders-page padding">
        <div class="container">
            <div class="row d-flex align-items-center mb-40">
                <div class="col-lg-8 col-md-6">
                    <div class="section-heading">
                        <h2>Закупки</h2>
                    </div>
                </div>
            </div>
            <div class="row orders-wrapper">
                <div class="col-lg-8">
                    @foreach ($orders as $order)
                        <div class="card border-0 shadow-sm order-card mb-5">
                            <div class="card-header border-bottom bg-white">
                                <div class="order-title">{{ $order->title }}</div>
                                <div class="order-date" title="Создано {{ $order->date }}. Обновлено {{ $order->update }}">{{ $order->date }}</div>
                            </div>
                            <div class="card-body pb-0">
                                <div class="order-customer mb-3">
                                    <div class="order-label">Заказчик:</div>
                                    <div class="order-customer-name">{{ $order->customer }}</div>
                                </div>
                                <div class="order-description mb-3">
                                    <div class="order-label">Описание:</div>
                                    <div class="order-description-value">
                                        {!! $order->description !!}
                                    </div>
                                </div>
                                <div class="order-contacts">
                                    <div class="order-label">Контакты:</div>
                                    <div class="order-contacts-value">{{ $order->contacts }}</div>
                                </div>
                            </div>
                            <div class="card-footer bg-white border-0 pb-3">
                                <div class="order-file text-md-right mb-3">
                                    <a href="/storage/{{ $order->filename }}" class="btn btn-primary" download>Скачать техническое задание</a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>
@endsection