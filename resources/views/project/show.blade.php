@extends('layouts.app')

@section('content')
    <section class="project-single-section py-50">
        <div class="container">
            <div class="breadcrumbs">
                <ul>
                    <li><a href="/">Главная</a></li>
                    <li><a href="{{ route('projects') }}">Проекты</a></li>
                    <li>{{ $project->name }}</li>
                </ul>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <h1 class="project-title mb-30">{{ $project->name }}</h1>
                    <h3 class="project-subtitle mb-20">Алматы, {{ $project->district }} район, {{ $project->address }}</h3>
                </div>
                <div class="col-md-6">
                    <div class="project-price">от {{ price_format($project->startPrice) }}</div>
                </div>
            </div>
            <div class="row project-single-wrap align-items-center">
                <div class="col-md-12 sm-padding">
                    <div id="project-single-carousel" class="project-single-carousel box-shadow owl-carousel">
                        @foreach($project->galleryImages as $image)
                            <div class="single-carousel">
                                <img src="/storage/{{ $image->img }}" alt="img">
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="col-md-12 mt-70">
                    <div class="project-single-content padding-20">
                        <h3>Характеристики</h3>
                        <hr style="border-color:#f6f6f6;">
                        <div class="row">
                            <div class="col-md-3 mb-4">
                                <i class="flaticon-3d-buildings"></i>
                                <div class="project-houses project-parameter">
                                    <strong>{{ $project->houses }}</strong>
                                    <div class="param-text">Количество домов</div>
                                </div>
                            </div>
                            <div class="col-md-3 mb-4">
                                <i class="flaticon-handrail"></i>
                                <div class="project-floors project-parameter">
                                    <strong>{{ $project->floors }}</strong>
                                    <div class="param-text">Этажей</div>
                                </div>
                            </div>
                            <div class="col-md-3 mb-4">
                                <i class="flaticon-rating"></i>
                                <div class="project-class project-parameter">
                                    <strong>{{ $project->class }}</strong>
                                    <div class="param-text">Класс</div>
                                </div>
                            </div>
                            <div class="col-md-3 mb-4">
                                <i class="flaticon-trowel"></i>
                                <div class="project-technology project-parameter">
                                    <strong>{{ $project->technology }}</strong>
                                    <div class="param-text">Технология</div>
                                </div>
                            </div>
                            <div class="col-md-3 mb-4">
                                <i class="flaticon-heater"></i>
                                <div class="project-heating project-parameter">
                                    <strong>{{ $project->heating }}</strong>
                                    <div class="param-text">Отопление</div>
                                </div>
                            </div>
                            <div class="col-md-3 mb-4">
                                <i class="flaticon-height"></i>
                                <div class="project-height project-parameter">
                                    <strong>{{ $project->height }} м</strong>
                                    <div class="param-text">Высота потолков</div>
                                </div>
                            </div>
                            <div class="col-md-3 mb-4">
                                <i class="flaticon-plan"></i>
                                <div class="project-rooms project-parameter">
                                    <strong>{{ $project->rooms }}</strong>
                                    <div class="param-text">Количество комнат</div>
                                </div>
                            </div>
                            <div class="col-md-3 mb-4">
                                <i class="flaticon-trowel-1"></i>
                                <div class="project-state project-parameter">
                                    <strong>{{ $project->state }}</strong>
                                    <div class="param-text">Состояние</div>
                                </div>
                            </div>
                            <div class="col-md-3 mb-4">
                                <i class="flaticon-parking-square-sign"></i>
                                <div class="project-parking project-parameter">
                                    <strong>{{ $project->parking }}</strong>
                                    <div class="param-text">Парковка</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 mt-50">
                    <div class="project-single-content padding-20">
                        <h3>Планировки</h3>
                        <hr style="border-color:#f6f6f6;">
                        <div class="row pb-5">
                            @foreach ($project->flats as $flat)
                                <div class="col-md-3 mb-4">
                                    <div class="card flat-card">
                                        <a href="{{ route('flat', [$project->slug, $flat->id]) }}">
                                            <img src="{{ asset('/storage/' . $flat->img) }}" height="200" alt="{{ $flat->title }}" class="flat-img card-img-top py-3">
                                        </a>
                                        <div class="card-body pt-2 pb-3 border-top">
                                            <h3 class="flat-title"><a href="{{ route('flat', [$project->slug, $flat->id]) }}">{{ $flat->title }}</a></h3>
                                            <div class="d-flex justify-content-between">
                                                <div class="flat-price">{{ price_format($flat->price) }}</div>
                                                <div>{{ $flat->square }} м²</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
