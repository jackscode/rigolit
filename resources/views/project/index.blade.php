@extends('layouts.app')

@section('content')
    <section class="projects-section padding">
        <div class="container">
            <div class="row d-flex align-items-center mb-40">
                <div class="col-lg-8 col-md-6">
                    <div class="section-heading">
                        <h2>Проекты</h2>
                    </div>
                </div>
            </div>
            <div class="row projects-wrapper">
                @foreach ($projects as $project)
                <div class="col-md-4 mb-50">
                    <div class="card shadow border-0 project-card">
                        <a href="/projects/{{ $project->slug }}">
                            <div class="card-header project-img" style="background-image: url(/storage/{{ $project->img }})"></div>
                        </a>
                        <div class="card-body">
                            <div class="project-name">
                                <a href="/projects/{{ $project->slug }}">{{ $project->name }}</a>
                            </div>
                            <div class="project-address">{{ $project->address }}</div>
                            <div class="project-price">от {{ number_format($project->startSquarePrice, 0, ',', ' ') }} тг/м²</div>
                            <a href="/projects/{{ $project->slug }}" class="project-show-more">Смотреть планировки</a>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </section>
@endsection