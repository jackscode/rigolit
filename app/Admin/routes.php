<?php

use Illuminate\Routing\Router;

Admin::routes();

Route::group([
    'prefix'        => config('admin.route.prefix'),
    'namespace'     => config('admin.route.namespace'),
    'middleware'    => config('admin.route.middleware'),
    'as'            => config('admin.route.prefix') . '.',
], function (Router $router) {

    $router->get('/', function () {
        return redirect('/admin/projects');
    })->name('home');

    $router->resource('/projects', ProjectController::class);
    $router->resource('/vacancies', VacancyController::class);
    $router->resource('/orders', OrderController::class);
    $router->resource('/digits', DigitController::class);
    $router->resource('/settings', SettingController::class);
});