<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->text('description');
            $table->string('address');
            $table->string('district')->nullable();
            $table->integer('houses');
            $table->integer('floors');
            $table->string('class')->nullable();
            $table->string('technology')->nullable();
            $table->string('heating')->nullable();
            $table->float('height')->nullable();
            $table->integer('rooms')->nullable();
            $table->string('state')->nullable();
            $table->string('parking')->nullable();
            $table->string('ending_date');
            $table->string('img');
            $table->string('slug');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
    }
}
