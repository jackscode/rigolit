<?php

if (!function_exists('price_format')) {
    function price_format($n) {
        // first strip any formatting;
        $n = (0+str_replace(",", "", $n));

        // is this a number?
        if (!is_numeric($n)) return false;

        // now filter it;
        if ($n > 1000000000000) {
            $new = round(($n/1000000000000), 2);
            return number_format($new, 2, ',', ',').' трлн тг';
        } elseif ($n > 1000000000) {
            $new = round(($n/1000000000), 2);
            return number_format($new, 2, ',', ',').' млрд тг';
        } elseif ($n > 1000000) {
            $new = round(($n/1000000), 2);
            return number_format($new, 2, ',', ',').' млн тг';
        } elseif ($n > 1000) {
            $new = round(($n/1000), 2);
            return number_format($new, 2, ',', ',').' тыс тг';
        }

        return number_format($n);
    }
}