@extends('layouts.app')

@section('content')
    <section class="service-section section-2 padding">
        <div class="container">
            <div class="row d-flex align-items-center">
                <div class="col-lg-5 sm-padding">
                    <h1 class="mb-4">О компании</h1>
                    <div class="service-content">
                        <p>Компания «Риголит» имеет большой накопленный опыт работы в сфере строительства и дальнейшего управления объектами, что позволило нам зарекомендовать себя как надежного и стабильного партнера.</p>
                        <p>За последние годы были реализованы десятки проектов в сфере социальной, коммерческой и жилой недвижимости. Всего текущий портфель реализованных проектов компании включает 21 объект недвижимости различного назначения общей площадью порядка 35 тыс. квадратных метров.</p>
                        <a href="{{ route('projects') }}" class="default-btn">Смотреть проекты</a>
                    </div>
                </div>
                <div class="col-lg-7 sm-padding">
                    <div class="row services-list">
                        <div class="col-md-6 padding-15">
                            <div class="service-item hexagon box-shadow">
                                <i class="flaticon-technology"></i>
                                <h3>Современные технологии</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt.</p>
                            </div>
                        </div>
                        <div class="col-md-6 padding-15 offset-top">
                            <div class="service-item hexagon box-shadow">
                                <i class="flaticon-winner"></i>
                                <h3>Независимость</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt.</p>
                            </div>
                        </div>
                        <div class="col-md-6 padding-15">
                            <div class="service-item hexagon box-shadow">
                                <i class="flaticon-conveyor-belt"></i>
                                <h3>Собственное производство</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt.</p>
                            </div>
                        </div>
                        <div class="col-md-6 padding-15 offset-top">
                            <div class="service-item hexagon box-shadow">
                                <i class="flaticon-energy"></i>
                                <h3>Энергетика</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="content-section padding">
        <div class="container">
            <div class="row content-wrap">
                <div class="col-lg-6 sm-padding">
                    <div class="content-info">
                        <h2>Сертификаты</h2>
                        <p>Наша компания прошла все проверки и официально сертифицирирована.
                            Имеется сертификат соответствия, который удостоверяет, что система экологического менеджмента
                            применительно к строительно-монтажным работам соответствует требованиям СТ РК ISO "Системы экологического менеджмента".</p>
                    </div>
                </div>
                <div class="col-lg-6 sm-padding">
                    <a href="{{ asset('cert/Сертификат1.jpg') }}" target="_blank">
                        <img class="box-shadow" src="{{ asset('cert/Сертификат1.jpg') }}" alt="img">
                    </a>
                </div>
            </div>
        </div>
    </section>
@endsection