@extends('layouts.app', [
    'seo' => false,
])

@section('content')
    <section class="vacancy-page padding {{ !$vacancy->active ? 'vacancy-inactive' : '' }}" style="padding-top:50px;">
        <div class="container">
            <div class="row d-flex align-items-center mb-40">
                <div class="col-md-12 mb-3">
                    <div class="breadcrumbs px-0" style="background-color:transparent">
                        <ul>
                            <li><a href="/">Главная</a></li>
                            <li><a href="{{ route('vacancies') }}">Вакансии</a></li>
                            <li>{{ $vacancy->title }}</li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-8 col-md-6">
                    <div class="section-heading">
                        <h1>{{ $vacancy->title }}</h1>
                        @if (!$vacancy->active)
                        <div class="alert alert-warning mt-4 py-2">
                            Вакансия в архиве
                        </div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-7 col-sm-12 mb-20">
                    <div class="vacancy-content d-flex justify-content-between mb-3">
                        <div class="vacancy-param">
                            <div class="vacancy-param-text">Занятость</div>
                            <div class="vacancy-param-value">
                                {{ $vacancy->employment }}
                            </div>
                        </div>
                        <div class="vacancy-param">
                            <div class="vacancy-param-text">Зарплата</div>
                            <div class="vacancy-param-value">
                                {{ $vacancy->salary ?? '' }}
                            </div>
                        </div>
                        <div class="vacancy-param">
                            <div class="vacancy-param-text">Город</div>
                            <div class="vacancy-param-value">
                                {{ $vacancy->city }}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-7 col-sm-12">
                    @if (!empty($vacancy->experience))
                        <div class="vacancy-param mb-3">
                            <div class="vacancy-param-content">
                                Опыт работы: {{ $vacancy->experience }}
                            </div>
                        </div>
                    @endif
                    @if (!empty($vacancy->description))
                        <div class="vacancy-param mb-5">
                            <div class="vacancy-subtitle">Описание</div>
                            <div class="vacancy-param-content">
                                {!! $vacancy->description !!}
                            </div>
                        </div>
                    @endif
                    <div class="vacancy-param mb-5">
                        <div class="vacancy-subtitle">Требования</div>
                        <div class="vacancy-param-content">
                            {!! $vacancy->requirements !!}
                        </div>
                    </div>
                    <div class="vacancy-param mb-5">
                        <div class="vacancy-subtitle">Обязанности</div>
                        <div class="vacancy-param-content">
                            {!! $vacancy->responsibilities !!}
                        </div>
                    </div>
                    @if (!empty($vacancy->conditions))
                    <div class="vacancy-param mb-5">
                        <div class="vacancy-subtitle">Условия</div>
                        <div class="vacancy-param-content">
                            {!! $vacancy->conditions !!}
                        </div>
                    </div>
                    @endif
                    <button class="btn btn-primary vacancy-btn">Откликнуться на вакансию</button>
                    <hr style="border-color:#f1f1f1;" class="mt-4">
                    <div class="text-muted vacancy-update float-right">Вакансия обновлялась {{ $vacancy->date }}</div>
                </div>
            </div>
        </div>
    </section>
@endsection