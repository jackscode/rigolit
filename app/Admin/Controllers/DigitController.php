<?php
namespace App\Admin\Controllers;

use App\Models\Digit;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class DigitController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Наши цифры';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid(): Grid
    {
        $grid = new Grid(new Digit);

        $grid->disableBatchActions();
        $grid->disableCreateButton();
        $grid->actions(function ($actions) {
            $actions->disableDelete();
            $actions->disableView();
        });

        $grid->column('id', __('ID'))->sortable();
        $grid->column('digit', 'Цифры');
        $grid->column('text', 'Текст');

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed   $id
     * @return Show
     */
    protected function detail($id): Show
    {
        $show = new Show(Digit::findOrFail($id));

        $show->field('id', __('ID'));
        $show->field('digit', 'Цифры');
        $show->field('text', 'Текст');
        $show->field('created_at', __('Дата создания'))->display(function ($date) {
            return date('Y-m-d H:i', strtotime($date));
        });
        $show->field('updated_at', __('Дата изменения'))->display(function ($date) {
            return date('Y-m-d H:i', strtotime($date));
        });

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form(): Form
    {
        $form = new Form(new Digit);

        $form->text('digit', 'Цифры')->required();
        $form->text('text', 'Текст')->required();

        return $form;
    }
}
