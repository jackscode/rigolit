@extends('layouts.app')

@section('content')
    <section class="vacancy-section padding">
        <div class="container">
            <div class="row d-flex align-items-center mb-40">
                <div class="col-lg-8 col-md-6">
                    <div class="section-heading">
                        <h2>Вакансии</h2>
                    </div>
                </div>
            </div>
            <div class="row projects-wrapper">
                @foreach ($vacancies as $vacancy)
                    <div class="col-md-9 mb-30">
                        <div class="card shadow-sm border-0 vacancy-card">
                            <div class="card-body">
                                <div class="d-flex justify-content-between mb-3">
                                    <div class="vacancy-title">
                                        <a href="/vacancy/{{ $vacancy->id }}">{{ $vacancy->title }}</a>
                                    </div>
                                    <div class="vacancy-salary">
                                        {{ $vacancy->salary ?? '' }}
                                    </div>
                                </div>
                                <div class="d-flex justify-content-between">
                                    <div class="vacancy-city">{{ $vacancy->city }}</div>
                                    <div class="vacancy-date">{{ $vacancy->date }}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
@endsection