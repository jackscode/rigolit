<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Jenssegers\Date\Date;

class Vacancy extends Model
{
    use HasFactory;

    public function getDateAttribute(): string
    {
        $date = Date::parse($this->updated_at);

        return $date->isToday() ? 'Сегодня' : $date->format('j F');
    }

    public function getRequirementsAttribute($requirements): string
    {
        return htmlspecialchars_decode($requirements);
    }

    public function getResponsibilitiesAttribute($responsibilities): string
    {
        return htmlspecialchars_decode($responsibilities);
    }
}
