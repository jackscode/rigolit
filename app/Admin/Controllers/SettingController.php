<?php
namespace App\Admin\Controllers;

use App\Models\Setting;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class SettingController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Настройки';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid(): Grid
    {
        $grid = new Grid(new Setting());

        $grid->column('id', __('ID'))->sortable();
        $grid->column('name', 'Название');
        $grid->column('value', 'Значение');

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed   $id
     * @return Show
     */
    protected function detail($id): Show
    {
        $show = new Show(Setting::findOrFail($id));

        $show->field('id', __('ID'));
        $show->field('name', 'Название');
        $show->field('key', 'Ключ');
        $show->field('value', 'Значение');
        $show->field('created_at', __('Дата создания'))->display(function ($date) {
            return date('Y-m-d H:i', strtotime($date));
        });
        $show->field('updated_at', __('Дата изменения'))->display(function ($date) {
            return date('Y-m-d H:i', strtotime($date));
        });

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form(): Form
    {
        $form = new Form(new Setting());

        $form->text('name', 'Название')->required();
        $form->text('key', 'Ключ')->required();
        $form->text('value', 'Значение')->required();

        return $form;
    }
}
