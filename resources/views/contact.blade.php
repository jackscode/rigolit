@extends('layouts.app')

@section('content')
    <section class="contact-section bg-grey padding border-top">
        <div class="container padding-20">
            <div class="contact-wrap row">
                <div class="col-md-5 padding-20">
                    <div class="contact-info">
                        <h2 class="page-heading mb-4">Контакты</h2>
                        <div class="mb-2">
                            <i class="flaticon-pin"></i> <span>{{ $address }}</span>
                        </div>
                        <div class="mb-2">
                            <i class="flaticon-phone-call"></i> <span>{{ $phone }}</span>
                        </div>
                        <div class="mb-2">
                            <i class="flaticon-envelope"></i> <span>{{ $email }}</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-7 padding-15">
                    <a class="dg-widget-link" href="http://2gis.kz/almaty/firm/70000001048786461/center/76.963695,43.239685/zoom/16?utm_medium=widget-source&utm_campaign=firmsonmap&utm_source=bigMap">Посмотреть на карте Алматы</a><div class="dg-widget-link"><a href="http://2gis.kz/almaty/center/76.963695,43.239685/zoom/16/routeTab/rsType/bus/to/76.963695,43.239685╎Риголит, компания?utm_medium=widget-source&utm_campaign=firmsonmap&utm_source=route">Найти проезд до Риголит, компания</a></div><script charset="utf-8" src="https://widgets.2gis.com/js/DGWidgetLoader.js"></script><script charset="utf-8">new DGWidgetLoader({"width":"100%","height":400,"borderColor":"transparent","pos":{"lat":43.239685,"lon":76.963695,"zoom":16},"opt":{"city":"almaty"},"org":[{"id":"70000001048786461"}]});</script><noscript style="color:#c00;font-size:16px;font-weight:bold;">Виджет карты использует JavaScript. Включите его в настройках вашего браузера.</noscript>
                </div>
            </div>
        </div>
    </section>
@endsection