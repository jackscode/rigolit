<?php

use App\Http\Controllers\FlatController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\ProjectController;
use App\Http\Controllers\VacancyController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [
    HomeController::class,
    'index',
]);

Route::get('/about', function () {
    return view('about');
})->name('about');

Route::get('/contact', function () {
    return view('contact');
})->name('contact');

Route::get('/projects', [
    ProjectController::class,
    'index',
])->name('projects');

Route::get('/projects/{slug}', [
    ProjectController::class,
    'show',
])->name('project');

Route::get('/projects/{slug}/flat/{id}', [
    FlatController::class,
    'show'
])->name('flat');

Route::get('/orders', [
    OrderController::class,
    'index',
])->name('orders');

Route::get('/order/{id}', [
    OrderController::class,
    'show',
])->name('order');

Route::get('/vacancies', [
    VacancyController::class,
    'index',
])->name('vacancies');

Route::get('/vacancy/{id}', [
    VacancyController::class,
    'show'
])->name('vacancy');