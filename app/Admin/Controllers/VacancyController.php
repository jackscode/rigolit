<?php

namespace App\Admin\Controllers;

use App\Models\Vacancy;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class VacancyController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Вакансии';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Vacancy());

        $grid->column('id', __('ID'))->sortable();
        $grid->column('title', 'Название');
        $grid->column('created_at', __('Дата создания'))->display(function ($date) {
            return date('Y-m-d H:i', strtotime($date));
        });
        $grid->column('updated_at', __('Дата изменения'))->display(function ($date) {
            return date('Y-m-d H:i', strtotime($date));
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed   $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Vacancy::findOrFail($id));

        $show->field('id', __('ID'));
        $show->field('title');
        $show->field('created_at', __('Дата создания'))->display(function ($date) {
            return date('Y-m-d H:i', strtotime($date));
        });
        $show->field('updated_at', __('Дата изменения'))->display(function ($date) {
            return date('Y-m-d H:i', strtotime($date));
        });

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Vacancy());

        $form->text('title', 'Название')->required();
        $form->switch('active', 'Активно')->states([
            'on'  => ['value' => 1, 'text' => 'Да', 'color' => 'success'],
            'off' => ['value' => 0, 'text' => 'Нет', 'color' => 'danger'],
        ])->default(1);
        $form->text('experience', 'Опыт работы')->default('1-3 года')->required();
        $form->text('employment', 'Занятость')->default('Полная занятость, полный день')->required();
        $form->text('salary', 'Зарплата')->default('Договорная')->required();
        $form->text('city', 'Город')->default('Алматы')->required();
        $form->summernote('description', 'Описание');
        $form->summernote('requirements', 'Требования')->required();
        $form->summernote('responsibilities', 'Обязанности')->required();
        $form->summernote('conditions', 'Условия');

        return $form;
    }
}
