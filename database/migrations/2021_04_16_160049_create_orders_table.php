<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->string('title')->comment('Заголовок');
            $table->string('customer')->comment('Заявитель')->nullable();
            $table->string('customer_address')->comment('Адрес заявителя')->nullable();
            $table->string('object_name')->comment('Наименование производственного объекта')->nullable();
            $table->string('bin')->comment('ИИН/БИН');
            $table->text('description');
            $table->boolean('active')->default(true);
            $table->string('filename');
            $table->string('contacts');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
