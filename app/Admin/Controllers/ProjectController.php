<?php

namespace App\Admin\Controllers;

use App\Models\Project;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class ProjectController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Проекты';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid(): Grid
    {
        $grid = new Grid(new Project());
        $grid->tools(function ($tools) {
            /** @var \Encore\Admin\Grid\Tools $tools */
            $tools->disableFilterButton();
        });
        $grid->disablePerPageSelector();

        $grid->column('id', 'ID')->sortable();
        $grid->column('name', 'Название');
        $grid->column('created_at', __('Дата создания'))->display(function ($date) {
            return date('Y-m-d H:i', strtotime($date));
        });
        $grid->column('updated_at', __('Дата изменения'))->display(function ($date) {
            return date('Y-m-d H:i', strtotime($date));
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed   $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Project::findOrFail($id));
        $show->panel()->tools(function ($tools) {
            $tools->disableList();
        });

        $show->field('id', __('ID'));
        $show->field('name');
        $show->field('created_at', __('Дата создания'))->display(function ($date) {
            return date('Y-m-d H:i', strtotime($date));
        });
        $show->field('updated_at', __('Дата изменения'))->display(function ($date) {
            return date('Y-m-d H:i', strtotime($date));
        });

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Project());

        $form->text('name', 'Название')->required();
        $form->switch('active', 'Активно')->states([
            'on'  => ['value' => 1, 'text' => 'Да', 'color' => 'success'],
            'off' => ['value' => 0, 'text' => 'Нет', 'color' => 'danger'],
        ])->default(1);
        $form->text('district', 'Район')->required();
        $form->text('address', 'Адрес')->required();
        $form->number('houses', 'Количество домов');
        $form->number('floors', 'Количество этажей');
        $form->text('class', 'Класс')->required();
        $form->text('technology', 'Технология')->required();
        $form->text('heating', 'Отопление')->required();
        $form->number('height', 'Высота потолков')->required();
        $form->number('rooms', 'Максимальное количество комнат')->required();
        $form->text('state', 'Состояние')->required();
        $form->text('parking', 'Парковка')->required();
        $form->text('ending_date', 'Срок сдачи проекта')->required();
        $form->image('img', 'Картинка')->required();

        $form->hasMany('flats', 'Планировки', function (Form\NestedForm $form) {
            $form->text('title', 'Название')->required();
            $form->number('rooms', 'Комнаты')->required();
            $form->number('price', 'Стоимость')->required();
            $form->number('square_price', 'Стоимость за кв. м.')->required();
            $form->number('square', 'Площадь')->required();
            $form->image('img', 'Картинка')->required();
        });

        $form->hasMany('galleryimages', 'Галерея', function (Form\NestedForm $form) {
            $form->image('img', 'Картинка')->required();
        });

        return $form;
    }
}
