<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    use HasFactory;

    public function getStartPriceAttribute()
    {
        $flat = $this->flats()->where('rooms', '>', 0)->first();

        return $flat->price ?? 0;
    }

    public function getStartSquarePriceAttribute()
    {
        $flat = $this->flats()->where('rooms', '>', 0)->first();

        return $flat->square_price ?? 0;
    }

    public function flats()
    {
        return $this->hasMany(Flat::class);
    }

    public function galleryImages()
    {
        return $this->hasMany(GalleryImage::class);
    }
}
