<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use Jenssegers\Date\Date;

class Order extends Model
{
    use HasFactory;

    public function getDateAttribute(): string
    {
        $date = Date::parse($this->created_at);

        return $date->isToday() ? Carbon::createFromTimeStamp(strtotime($this->created_at))->diffForHumans() : $date->format('j F');
    }

    public function getUpdateAttribute(): string
    {
        $date = Date::parse($this->updated_at);

        return $date->isToday() ? Carbon::createFromTimeStamp(strtotime($this->updated_at))->diffForHumans() : $date->format('j F');
    }
}
