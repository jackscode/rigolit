<?php

namespace App\Http\Controllers;

use App\Models\Flat;
use App\Models\Project;
use Illuminate\Http\Request;

class FlatController extends Controller
{
    public function show(Request $request)
    {
        $project = Project::query()->where('slug', $request->slug)->first();

        if (!$project) {
            abort(404);
        }

        $flat = Flat::query()->where([
            ['id', '=', $request->id],
            ['project_id', '=', $project->id]
        ])->first();

        return view('flat.show', compact(['project', 'flat']));
    }
}
