@extends('layouts.app')

@section('content')
    <div id="main-slider" class="dl-slider">
        <div class="single-slide">
            <div class="bg-img" style="background-image: url('/img/rigolit1.png');"></div>
            <div class="overlay"></div>
            <div class="slider-content-wrap d-flex align-items-center text-left">
                <div class="container">
                    <div class="slider-content">
                        <div class="dl-caption medium">
                            <div class="inner-layer">
                                <div data-animation="fade-in-top" data-delay=".2s">{!! $slider_h2 !!}</div>
                            </div>
                        </div>
                        <div class="dl-caption big">
                            <div class="inner-layer">
                                <div data-animation="fade-in-left" data-delay=".3s">{!! $slider_h1 !!}</div>
                            </div>
                        </div>
                        <div class="dl-btn-group">
                            <div class="inner-layer">
                                <a href="#" class="dl-btn" id="go-to-projects" data-animation="fade-in-left" data-delay=".5s">Подробнее</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <section class="about-section padding">
        <div class="container">
            <div class="row about-wrap about-promo">
                <div class="col-md-4 about-promo-item mb-5">
                    <i class="flaticon-grid"></i>
                    <div class="digits-item">
                        <div class="digits">{{ $digits[0]->digit ?? '' }}</div>
                        <div class="digits-text">{{ $digits[0]->text ?? '' }}</div>
                    </div>
                </div>
                <div class="col-md-4 about-promo-item mb-5">
                    <i class="flaticon-calendar"></i>
                    <div class="digits-item">
                        <div class="digits">{{ $digits[1]->digit ?? '' }}</div>
                        <div class="digits-text">{{ $digits[1]->text ?? '' }}</div>
                    </div>
                </div>
                <div class="col-md-4 about-promo-item mb-5">
                    <i class="flaticon-3d-buildings"></i>
                    <div class="digits-item">
                        <div class="digits">{{ $digits[2]->digit ?? '' }}</div>
                        <div class="digits-text">{{ $digits[2]->text ?? '' }}</div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="projects-section padding">
        <div class="container">
            <div class="row d-flex align-items-center mb-40 justify-content-center">
                <div class="col-lg-8 col-md-6">
                    <div class="section-heading">
                        <a href="{{ route('projects') }}"><h2>Проекты</h2></a>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 text-right d-none d-md-block d-lg-block">
                    <a href="{{ route('projects') }}" class="btn btn-primary">Посмотреть все</a>
                </div>
            </div>
            <div class="row projects-wrapper">
                @foreach ($projects as $project)
                    <div class="col-md-4 mb-50">
                        <div class="card shadow border-0 project-card">
                            <a href="/projects/{{ $project->slug }}">
                                <div class="card-header project-img" style="background-image: url(/storage/{{ $project->img }})"></div>
                            </a>
                            <div class="card-body">
                                <div class="project-name">
                                    <a href="/projects/{{ $project->slug }}">{{ $project->name }}</a>
                                </div>
                                <div class="project-address">{{ $project->address }}</div>
                                <div class="project-price">от {{ number_format($project->startSquarePrice, 0, ',', ' ') }} тг/м²</div>
                                <a href="/projects/{{ $project->slug }}" class="project-show-more">Смотреть планировки</a>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
{{--    <div class="sponsor-section bg-grey">--}}
{{--        <div class="container">--}}
{{--            <div id="sponsor-carousel" class="sponsor-carousel owl-carousel">--}}
{{--                <div class="sponsor-item">--}}
{{--                    <img src="img/sponsor1.png" alt="sponsor">--}}
{{--                </div>--}}
{{--                <div class="sponsor-item">--}}
{{--                    <img src="img/sponsor2.png" alt="sponsor">--}}
{{--                </div>--}}
{{--                <div class="sponsor-item">--}}
{{--                    <img src="img/sponsor3.png" alt="sponsor">--}}
{{--                </div>--}}
{{--                <div class="sponsor-item">--}}
{{--                    <img src="img/sponsor4.png" alt="sponsor">--}}
{{--                </div>--}}
{{--                <div class="sponsor-item">--}}
{{--                    <img src="img/sponsor5.png" alt="sponsor">--}}
{{--                </div>--}}
{{--                <div class="sponsor-item">--}}
{{--                    <img src="img/sponsor6.png" alt="sponsor">--}}
{{--                </div>--}}
{{--                <div class="sponsor-item">--}}
{{--                    <img src="img/sponsor7.png" alt="sponsor">--}}
{{--                </div>--}}
{{--                <div class="sponsor-item">--}}
{{--                    <img src="img/sponsor8.png" alt="sponsor">--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
@endsection