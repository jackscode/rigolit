<?php

namespace App\Providers;

use App\Models\Digit;
use App\Models\Setting;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use Jenssegers\Date\Date;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        Date::setlocale(config('app.locale'));

        if (!Schema::hasTable('settings')) {
            return;
        }

        $settings    = Setting::all()->toArray();
        $phone       = '+7 (747) 964-43-42';
        $address     = 'ул. Бегалина, д. 141/16, Алматы, Казахстан';
        $email       = 'info@rigolit.kz';
        $slider_h1   = 'Комфортабельные квартиры с видом на горы';
        $slider_h2   = 'Профессиональный подход - четкие решения';
        $footer_text = 'Комфортабельные квартиры с видом на горы.';

        foreach ($settings as $setting) {
            if ($setting['key'] === 'phone') {
                $phone = $setting['value'];
            }

            if ($setting['key'] === 'address') {
                $address = $setting['value'];
            }

            if ($setting['key'] === 'email') {
                $email = $setting['value'];
            }

            if ($setting['key'] === 'slider_h1') {
                $slider_h1 = $setting['value'];
            }

            if ($setting['key'] === 'slider_h2') {
                $slider_h2 = $setting['value'];
            }

            if ($setting['key'] === 'footer_text') {
                $footer_text = $setting['value'];
            }
        }

        View::share('phone', $phone);

        View::share('address', $address);

        View::share('email', $email);

        View::share('slider_h2', $slider_h2);

        View::share('slider_h1', $slider_h1);

        View::share('footer_text', $footer_text);

        if (Schema::hasTable('digits')) {
            $digits = Digit::all();

            if (!$digits || $digits->isEmpty()) {
                $digits = collect([
                    [
                        'digit' => '730 000 м²',
                        'text'  => 'построенных помещений',
                    ],
                    [
                        'digit' => '10 лет',
                        'text'  => 'на рынке',
                    ],
                    [
                        'digit' => '12 жилых',
                        'text'  => 'комплексов',
                    ],
                ]);
            }

            View::share('digits', $digits);
        }
    }
}
