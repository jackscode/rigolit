<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Строительная компания Риголит Алматы">
    <meta name="author" content="jackscode.kz">
    @if(isset($seo) && !$seo)
        <meta name="robots" content="noindex, nofollow">
    @endif
    <title>Риголит | Строительная компания</title>
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('favicon.png') }}">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;500;600;700;800&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/fontawesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/themify-icons.css') }}">
    <link rel="stylesheet" href="{{ asset('css/elegant-line-icons.css') }}">
    <link rel="stylesheet" href="{{ asset('css/elegant-font-icons.css') }}">
    <link rel="stylesheet" href="{{ asset('css/flaticon.css') }}">
    <link rel="stylesheet" href="{{ asset('css/animate.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/slick.css') }}">
    <link rel="stylesheet" href="{{ asset('css/slider.css') }}">
    <link rel="stylesheet" href="{{ asset('css/odometer.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/venobox/venobox.css') }}">
    <link rel="stylesheet" href="{{ asset('css/owl.carousel.css') }}">
    <link rel="stylesheet" href="{{ asset('css/main.css') }}">
    <link rel="stylesheet" href="{{ asset('css/responsive.css') }}">
    <script src="{{ asset('js/vendor/modernizr-2.8.3-respond-1.4.2.min.js') }}"></script>
</head>
<body class="color-{{ $_GET['color'] ?? 'default' }}">
    <!--[if lt IE 8]>
    <p class="browserupgrade">Вы используете древний браузер. Пожалуйста, <a href="http://browsehappy.com/">обновите свой браузер</a>.</p>
    <![endif]-->
    <div class="site-preloader-wrap">
        <div class="spinner"></div>
    </div>
    <header class="header">
        <div class="primary-header">
            <div class="container">
                <div class="primary-header-inner">
                    <div class="header-logo">
                        <a href="/"><img src="{{ asset('img/logo.png') }}" alt="Риголит"></a>
                    </div>
                    <div class="header-menu-wrap">
                        <ul class="dl-menu">
                            <li><a href="/">Главная</a></li>
                            <li><a href="{{ route('about') }}">О компании</a></li>
                            <li><a href="{{ route('projects') }}">Проекты</a></li>
                            <li><a href="{{ route('orders') }}">Закупки</a></li>
                            <li><a href="{{ route('contact') }}">Контакты</a></li>
                        </ul>
                    </div>
                    <div class="header-right">
                        <div class="mobile-menu-icon">
                            <div class="burger-menu">
                                <div class="line-menu line-half first-line"></div>
                                <div class="line-menu"></div>
                                <div class="line-menu line-half last-line"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>

    @yield('content')

    <section class="widget-section padding">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-sm-6 sm-padding">
                    <div class="widget-content">
                        <a href="/" class="d-flex align-items-center mb-20">
                            <img src="/favicon.png" alt="brand" style="width:100px;margin-left:-12px;">
                            <div class="text-center">
                                <span class="footer-rigolit-title">Риголит</span>
                                <span class="footer-rigolit-subtitle">строительная компания</span>
                            </div>
                        </a>
                        <p>{!! $footer_text !!}</p>
                    </div>
                </div>
                <div class="col-lg-2 col-sm-6 sm-padding">
                    <div class="widget-content">
                        <h4>Компания</h4>
                        <ul class="widget-links">
                            <li><a href="{{ route('projects') }}">Проекты</a></li>
                            <li><a href="{{ route('orders') }}">Закупки</a></li>
                            <li><a href="{{ route('vacancies') }}">Вакансии</a></li>
                            <li><a href="{{ route('contact') }}">Контакты</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6 sm-padding">
                    <div class="widget-content">
                        <h4>Адрес</h4>
                        <p>{{ $address }}</p>
                        <span>{{ $email }}</span>
                        <span>{{ $phone }}</span>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <a data-scroll href="#header" id="scroll-to-top"><i class="arrow_carrot-up"></i></a>
    <script src="{{ asset('js/vendor/jquery-1.12.4.min.js') }}"></script>
    <script src="{{ asset('js/vendor/bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/vendor/tether.min.js') }}"></script>
    <script src="{{ asset('js/vendor/headroom.min.js') }}"></script>
    <script src="{{ asset('js/vendor/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('js/vendor/smooth-scroll.min.js') }}"></script>
    <script src="{{ asset('js/vendor/venobox.min.js') }}"></script>
    <script src="{{ asset('js/vendor/jquery.ajaxchimp.min.js') }}"></script>
    <script src="{{ asset('js/vendor/slick.min.js') }}"></script>
    <script src="{{ asset('js/vendor/waypoints.min.js') }}"></script>
    <script src="{{ asset('js/vendor/odometer.min.js') }}"></script>
    <script src="{{ asset('js/vendor/wow.min.js') }}"></script>
    <script src="{{ asset('js/main.js') }}"></script>
</body>
</html>
