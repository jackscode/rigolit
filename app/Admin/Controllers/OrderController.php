<?php

namespace App\Admin\Controllers;

use App\Models\Order;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class OrderController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Закупки';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Order());

        $grid->column('id', __('ID'))->sortable();
        $grid->column('title', 'Название');
        $grid->column('created_at', __('Дата создания'))->display(function ($date) {
            return date('Y-m-d H:i', strtotime($date));
        });
        $grid->column('updated_at', __('Дата изменения'))->display(function ($date) {
            return date('Y-m-d H:i', strtotime($date));
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed   $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Order::findOrFail($id));

        $show->field('id', __('ID'));
        $show->field('title');
        $show->field('created_at', __('Дата создания'))->display(function ($date) {
            return date('Y-m-d H:i', strtotime($date));
        });
        $show->field('updated_at', __('Дата изменения'))->display(function ($date) {
            return date('Y-m-d H:i', strtotime($date));
        });

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Order());

        $form->text('title', 'Название')->required();
        $form->switch('active', 'Активно')->states([
            'on'  => ['value' => 1, 'text' => 'Да', 'color' => 'success'],
            'off' => ['value' => 0, 'text' => 'Нет', 'color' => 'danger'],
        ])->default(1);
        $form->text('customer', 'Заявитель')->required();
        $form->text('customer_address', 'Адрес заявителя')->required();
        $form->text('bin', 'ИИН/БИН')->required();
        $form->text('object_name', 'Название объекта')->required();
        $form->textarea('description', 'Описание');
        $form->text('contacts', 'Контакты')->required();
        $form->file('filename', 'Файл')->required();

        return $form;
    }
}
