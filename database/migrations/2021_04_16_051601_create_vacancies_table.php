<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVacanciesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vacancies', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->boolean('active')->default(1);
            $table->text('requirements')->comment('Требования')->nullable();
            $table->text('responsibilities')->comment('Обязанности')->nullable();
            $table->string('salary')->comment('Зарплата')->nullable();
            $table->text('description')->comment('Описание')->nullable();
            $table->string('city')->nullable()->default('Алматы');
            $table->string('employment')->comment('Занятость')->nullable()->default('Полная');
            $table->string('experience')->comment('Опыт работы')->nullable()->default('1-3 года');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vacancies');
    }
}
