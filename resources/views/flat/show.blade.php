@extends('layouts.app')

@section('content')
    <section class="project-single-section padding" style="padding-top: 50px;">
        <div class="container">
            <div class="breadcrumbs">
                <ul>
                    <li><a href="/">Главная</a></li>
                    <li><a href="{{ route('projects') }}">Проекты</a></li>
                    <li><a href="{{ route('project', $project->slug) }}">{{ $project->name }}</a></li>
                    <li>{{ $flat->title }}</li>
                </ul>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <h1 class="project-title mb-30">{{ $flat->title }} на "{{ $project->name }}"</h1>
                    <h3 class="project-subtitle mb-20">Алматы, {{ $project->district }} район, {{ $project->address }}</h3>
                </div>
            </div>
            <div class="row project-single-wrap">
                <div class="col-md-7 sm-padding">
                    <div class="flat-main-img shadow-sm">
                        <img src="{{ asset('/storage/' . $flat->img) }}" alt="img" width="50%">
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="flat-details shadow-sm">
                        <div class="h2 price">{{ number_format($flat->price, 0, ',', ' ') }} тг</div>
                        <hr style="border-color:#f5f5f5;">
                        <div class="col-md-12 mb-4">
                            <i class="flaticon-3d-buildings"></i>
                            <div class="project-houses project-parameter">
                                <strong>{{ $flat->square }} м²</strong>
                                <div class="param-text">площадь квартиры</div>
                            </div>
                        </div>
                        <div class="col-md-12 mb-4">
                            <i class="flaticon-handrail"></i>
                            <div class="project-floors project-parameter">
                                <strong>{{ number_format($flat->square_price, 0, ',', ' ') }} тг/м²</strong>
                                <div class="param-text">цена за квадратный метр</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 mt-70">
                    <div class="project-single-content padding-20">
                        <h3>Другие планировки</h3>
                        <hr style="border-color:#f6f6f6;">
                        <div class="row">
                            @foreach ($project->flats as $projectFlat)
                                @if ($flat->id !== $projectFlat->id)
                                    <div class="col-md-3 mb-4">
                                        <div class="card flat-card">
                                            <a href="{{ route('flat', [$project->slug, $projectFlat->id]) }}">
                                                <img src="{{ asset('/storage/' . $projectFlat->img) }}" height="200" alt="{{ $projectFlat->title }}" class="flat-img card-img-top py-3">
                                            </a>
                                            <div class="card-body pt-2 pb-3 border-top">
                                                <h3 class="flat-title"><a href="">{{ $projectFlat->title }}</a></h3>
                                                <div class="d-flex justify-content-between">
                                                    <div class="flat-price">{{ price_format($projectFlat->price) }}</div>
                                                    <div>{{ $projectFlat->square }} м²</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection