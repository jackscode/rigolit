<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Flat extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'rooms',
        'price',
        'square_price',
        'square',
        'img'
    ];

    protected $table = 'project_flats';

    public function project()
    {
        return $this->belongsTo(Project::class);
    }
}
